## Practice of automated testing

* factory with traits
* test validations
* matcher from shoulda-matcher gem
* test scopes
* test double 
* test coverage with simplecov gem
* test todo_controller requests
* rubocop

