FactoryBot.define do
  factory :todo do
    title { 'The 1st ToDo' }
    description { "ToDo's description" }
    status { true }
    due_date { Time.current + 24.hours }
    
    trait :status_false do
      status { false }
    end        
    
    trait :overdue do
      due_date { Time.current - 24.hours }
    end
  end
end
