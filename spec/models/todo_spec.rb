require 'rails_helper'

RSpec.describe Todo, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:title) }  
    it { should validate_presence_of(:due_date) }      
  end  

  describe '#can_destroy?' do
    context 'when todo is overdued' do
      let(:todo){ create :todo, :overdue }
      
      it 'returns false' do
        expect(todo.can_destroy?).to be false
      end
    end
    
    context 'when todo is not overdued' do
      let(:todo){ create :todo }
      
      it 'returns true' do
        expect(todo.can_destroy?).to be true
      end
    end         
  end    
  
  describe '#complete' do
    let(:todo){ build(:todo) }
    
    before do
      allow(GreetingsLogger).to receive(:call)
      todo.complete
    end
    
    it 'calls GreetingsLogger' do
      expect(GreetingsLogger).to have_received(:call).with(no_args)
    end
  
    context 'when todo has any statuses' do
      let(:todo){ create :todo }
      
      it 'changes status to true' do
        todo.complete
        expect(todo.status).to be true
      end
    end
  end   
  
  describe '#activate' do
    context 'when todo has any statuses' do
      let(:todo){ create :todo }
      
      it 'changes status to false' do
        todo.activate
        expect(todo.status).to be false
      end
    end
  end  
  
  describe 'Scopes' do
    describe '.completed' do
      let(:todo) { Todo.completed }

      let(:todo_status_true) { create(:todo) }
      let(:todo_status_false) { create(:todo, :status_false) }

      before do
        todo_status_true
        todo_status_false
      end

      it 'should return proper amount of records' do
        expect(todo.count).to eq(1)
      end

      it 'should return proper records' do
        expect(todo).to     include(todo_status_false)
        expect(todo).not_to include(todo_status_true)
      end
    end
    
    describe '.active' do
      let(:todo) { Todo.active }

      let(:todo_status_true) { create(:todo) }
      let(:todo_status_false) { create(:todo, :status_false) }

      before do
        todo_status_true
        todo_status_false
      end

      it 'should return proper amount of records' do
        expect(todo.count).to eq(1)
      end

      it 'should return proper records' do
        expect(todo).to     include(todo_status_true)
        expect(todo).not_to include(todo_status_false)
      end
    end    
  end     
end
