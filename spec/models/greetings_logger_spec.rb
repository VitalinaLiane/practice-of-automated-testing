require 'rails_helper'

RSpec.describe GreetingsLogger, type: :model do
  describe '.call' do
    it 'prints congratulations' do
      expect do
        GreetingsLogger.call
      end.to output("Congratulations with completed task!\n").to_stdout
    end

    it 'does not print not congratulations' do
      expect do
        GreetingsLogger.call
      end.to_not output("Not congratulations things\n").to_stdout
    end
  end
end
