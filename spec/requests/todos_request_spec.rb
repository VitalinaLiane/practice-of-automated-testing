require 'rails_helper'

RSpec.describe "Todos", type: :request do
  context 'GET index' do
    before do
      get '/todos'
    end   
      
    it 'renders the index template' do      
      expect(response).to render_template(:index)
    end
    
    it 'returns OK responce' do  
      expect(response).to have_http_status(:ok)
    end
  end  
  
  context 'POST create' do
    let(:show_path){ 'http://www.example.com/todos/1' }
    let(:todo){ build :todo }
    
    before do
      post "/todos", params: { todo: attributes_for(:todo) }
    end    
    
    it 'redirects to show path' do      
      expect(response).to redirect_to(show_path)
    end
    
    it 'returns 302 responce' do       
      expect(response.status).to eq(302)
    end

    it 'creates new todo' do           
      expect { post "/todos", params: { todo: attributes_for(:todo) } }.to change(Todo, :count).by(1)
    end
        
    it 'display info message about success creating' do
      follow_redirect!
      expect(response.body).to include('Todo was successfully created.')
    end     
  end  
  
  context 'GET show' do
    let(:todo){ create :todo }    
    before do
      get "/todos/#{todo.id}"
    end    
    
    it 'renders show template' do      
      expect(response).to render_template(:show)
    end
    
    it 'does not render new template' do      
      expect(response).not_to render_template(:new)
    end    
    
    it 'returns 200 responce' do       
      expect(response).to have_http_status(200)
    end 
  end      
end
